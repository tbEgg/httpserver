CC = gcc

all : testserver client
testserver : server.c
	$(CC) $^ -lpthread -o $@ -W
client : socket.c
	$(CC) $^ -o $@

clean:
	rm client testserver
.PHONY : clean all
