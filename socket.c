#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

#define SIZE 30

int GetLine(int sockfd, char buf[], int size)
{
    char c = '\0';
    int charNum = 0;

    while(charNum < size - 1)
    {
        int n = recv(sockfd, &c, 1, 0);
        if(n > 0)   // 传输成功
        {
            if(c == '\r' || c == '\n')
            {
                // 只要遇到\r，下一个字符默认为\n也就是行终止
                buf[charNum++] = '\n';
                break;
            }
            buf[charNum++] = c;
        }
        else
        {
            buf[charNum++] = '\n';
            break;
        }
    }
    buf[charNum] = '\0';

    return charNum;
}

int main()
{
    // 配置需要连接的服务器端套接字地址
    struct sockaddr_in name;
    name.sin_family = AF_INET;
    uint16_t port = 4568;   // 服务器监听的端口
    name.sin_port = htons(port);
    char *str = "127.0.0.1"; // 服务器地址
    inet_pton(AF_INET, str, &(name.sin_addr.s_addr));
    socklen_t nameLen = sizeof(name);

    for(int i = 0; i < 1; i++)
    {
        // 客户端建立套接字并连接到服务器端
        // 每次connect后并关闭套接字后重新建立
        int sock = socket(PF_INET, SOCK_STREAM, 0);
        if(connect(sock, (struct sockaddr *)&name, nameLen) == -1)
        {
            printf("%d : connect failed.\n", i);
        }

        char *buf = "GeT /test HTTP/1.0\r\n";
        if(send(sock, buf, strlen(buf) + 1, 0) == -1)
        {
            printf("send failed.\n");
            break;
        }
        printf("send succeed\n");

        char get[100];
        printf("test1\n");
        while(GetLine(sock, get, 100) != 0)
        {
            printf("line = %s\n", get);
        }

        // 延迟五秒便于观察
        printf("close\n");
        close(sock);
        
    }
    printf("close\n");
    return 0;
}