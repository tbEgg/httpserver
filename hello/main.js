var nodes = [
    {name:"a"},
    {name: "b"}
];
var links = [
    {source:0, target:1, value:1}
];

// svg container
var svg = d3.select("body")
    .append("svg")
    .attr("width", 960)
    .attr("height", 600);
var width = 960;
var height = 600;

// 力学模拟器
// 分配力的名称和算法
var simulation = d3.forceSimulation()
    .force("link", d3.forceLink())
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter());

simulation.force("center")
    .x(width / 2)
    .y(height / 2);

/*var color = d3.scaleOrdinal()
    .domain(d3.range(nodes.length))
    .range(d3.schemeCategory10);*/

// 用来显示文字的提示框
var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("font-size", "12px")
    .style("font-weight", "bold")
    .text("")

// 绘制节点元素
var l = svg.append("g")
    .selectAll("line")
    .data(links)
    .enter()
    .append("line");
setLinkAttr(l);
   
// 绘制边
var n = svg.append("g")
    .selectAll("circle")
    .data(nodes)
    .enter()
    .append("circle");
setNodeAttr(n);
    
    
// 每次tick时绘制力导向图
simulation.nodes(nodes)
    .on("tick", ticked);
simulation.force("link")
    .links(links)
    .distance(function(d) {
        return d.value * 100;
    });
    
function ticked() {
    l.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; })

    n.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; })
}

/*d3.json("http://localhost:3000/json").then(function(graph) {
    console.log(graph)
    
});*/

// 设置边属性
function setLinkAttr(link) {
    link.attr("stroke","black")
        .attr("stroke-width",1);
}

function appendLink(link, new_links) {
    links.push(new_links);      // links为全局变量，存储所有边
    link = link.data(links);
    link_enter = link.enter().append("line");
    setLinkAttr(link_enter);
    return link_enter.merge(link);
}

// 设置节点属性，添加监视器动作
function setNodeAttr(node) {
    node.attr("r", 15)
    .attr("fill", function(d, i) {
        // 填充色
        return "yellow";
    })
    .on("mouseover", function(d, i) {
        // 鼠标移至节点上时，修改填充色
        // 增加文字说明
        d3.select(this).attr("fill", "silver");
        return tooltip.style("visibility", "visible").text(d.name);
    })
    .on("mousemove", function(d, i) {
        // 设置文字说明的位置
        console.log(d);
        return tooltip.style("top", (d.y - 10) + "px").style("left", (d.x + 10) + "px");
    })
    .on("mouseout", function(d, i) {
        // 鼠标移出节点时，缓慢变回原色
        // 隐藏文字说明
        d3.select(this)
            .transition()
            .duration(500)
            .attr("fill", function(d) {
                return "yellow";
            });
        return tooltip.style("visibility", "hidden");
    })
    .on("dblclick", dblclicked)     // 鼠标双击动作
    // 设置节点可以被拖拽
    .call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended));
}

// 增添节点
// node：原节点元素
function appendNode(node, new_nodes) {
    nodes.push(new_nodes);  // nodes为全局变量，存储所有节点属性
    node = node.data(nodes);
    node_enter = node.enter().append("circle");     // 添加新元素
    setNodeAttr(node_enter);
    return node_enter.merge(node);
}

request = new XMLHttpRequest();
request.onreadystatechange = function() {
    if (request.readyState == 4) {
        let sfd = document.getElementById("sfd");
        sfd.innerText = request.response;

        l = appendLink(l, {source:0,target:2,value:1});
        n = appendNode(n, {name:"c"});
        
        simulation.nodes(nodes)
            .on("tick", ticked);
        simulation.force("link")
            .links(links)
            .distance(function(d) {
                return d.value * 100;
            });
    }
}

function hello() {
    console.log("ddhdhdhdhdhhd"); 
}

function dblclicked(d) {
    request.open("POST", "http://localhost:3000");
    request.send("test");
}

function dragstarted(d) {
    if (!d3.event.active) {
        simulation.alphaTarget(0.3).restart();
    }
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) {
        simulation.alphaTarget(0);
    }
    d.fx = null;
    d.fy = null;
}