#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>

#define SIZE 50
#define BUFSIZE 1024

char *DOCROOT = "/home/tbegg/Desktop/httpserver";

int GetLine(int sockfd, char *buf, int size);
void PrintInfo(char *buf, int size);
void AcceptRequest(void *arg);
int Get(int sockfd, char *url, char *query);
void BadRequest(int sockfd, char *Info);

void DoNothing() {}
void SendBody(int sockfd, char *file)
{
    FILE *f = fopen(file, "rb");
    if(f == NULL)
    {
        printf("file can not open\n");
        BadRequest(sockfd, "file can't open");
        return;
    }

    char buf[1024];
    printf("body:\n");
    while(!feof(f))
    {
        fgets(buf, sizeof(buf), f);
        send(sockfd, buf, strlen(buf), 0);
        printf("size = %d, len = %d\n", sizeof(buf), strlen(buf));
    }
    printf("body end\n");
    fclose(f);
}


void BadRequest(int sockfd, char *Info)
{
    char buf[BUFSIZE];
    memset(buf, 0, BUFSIZE);
    strcat(buf, "HTTP/1.1 400 BAD REQUEST\r\n");
    strcat(buf, "Content-type: text/html\r\n");
    char s[64];
    memset(s, 0, 64);
    sprintf(s, "Content-Length: %ld\r\n", strlen(Info) + 13);
    strcat(buf, s);
    strcat(buf, "\r\n");
    strcat(buf, "<h1>");
    strcat(buf, Info);
    strcat(buf, "</h1>\r\n");

    int n = 0;
    if((n = send(sockfd, buf, strlen(buf) + 1, 0)) < 0)
    {
        printf("Error in send(Badrequest)\n");
    }
}



/**
 * 响应get方法
 * 
 * 
 * 
 */
int Get(int sockfd, char *url, char *query)
{
    if(strstr(url, "/../") != NULL)
    {
        // 试图非法访问根目录以外文件
        BadRequest(sockfd, "URL ERROR(..)");
        return -1;
    }

    // 报文头部
    char buf[BUFSIZE];
    memset(buf, 0, BUFSIZE);
    strcat(buf, "HTTP/1.1 200 OK\r\n");
    strcat(buf, "Content-Type: text/plain\r\n");
    strcat(buf, "Content-Encoding: utf-8\r\n");
    strcat(buf, "Content-Language: zh-CN\r\n");

    // 找到url指定的文件路径
    char file[64];
    memset(file, 0, 64);
    strcat(file, DOCROOT);
    printf("loca = %d, str = %s\n", strstr(url, "/"), url);
    if(strcmp(url, "/") == 0)
    {
        // 如果为单纯的“/”，导向index页面
        strcat(file, "/bug集合");
    }
    else if(strstr(url, "/") != 0)
    {
        // 如果开头没有“/“，加上
        strcat(file, "/");
        strcat(file, url);
    }

    // 获取文件信息
    struct stat s;
    if(lstat(file, &s) < 0)
    {
        // 打开文件失败，可能是文件不存在或者。。。。
        BadRequest(sockfd, "File don't exist");
        return -1;
    }
    if((s.st_mode & S_IFMT) != S_IFREG)
    {
        // 判断是否为普通文件
        BadRequest(sockfd, "URL error, it isn't a file");
        return -1;
    }

    char content_length[64];
    sprintf(content_length, "Content-Length: %ld\r\n", s.st_size);
    strcat(buf, content_length);
    strcat(buf, "\r\n");

    if(send(sockfd, buf, strlen(buf), 0) < 0)
    {
        printf("GET : Error in send().\n");
        return -1;
    }

    SendBody(sockfd, file);
}

/**
 * 接收请求并进行相应的处理
 * 在每次收到请求时开新线程进行处理
 * 
 */
void AcceptRequest(void *arg)
{
    // 获得客户端套接字
    int client = *((int *)arg);
    char method[255];
    char url[255];
    char query[255];
    char buf[1024];

    // 从起始行中解析出方法，url等
    // 读入起始行，存入buf中
    int charNum = GetLine(client, buf, sizeof(buf));
    int i = 0;
    while(i < (int)(sizeof(method) - 1) && i < charNum - 1 && !isspace(buf[i]))
    {
        method[i] = buf[i];
        i++;
    }
    method[i] = '\0';

    // 检测是否是支持的方法
    if(strcasecmp(method, "GET"))
    {
        // 当两个字符串不相等时返回非0值
        BadRequest(client, "UNSUPPORTED METHOD");
        return;
    }

    // 读取文件路径
    // 当前的buf[i]显然是空格，所以直接检测之后的空格字符
    int j = 0;
    while(++i < charNum - 1 && isspace(buf[i]));
    while(i < charNum - 1 && j < (int)(sizeof(url) - 1) && !isspace(buf[i]) && buf[i] != '?')
    {
        url[j] = buf[i];
        i++;
        j++;
    }
    url[j] = '\0';
    j = 0;
    if(buf[i] == '?') // 检测后考虑下一位字符，？不需要
    {
        while(++i < charNum - 1 && j < (int)(sizeof(query) - 1) && !isspace(buf[i]))
        {
            query[j] = buf[i];
            j++;
        }
        query[j] = '\0';
    }

    printf("method = %s, url = %s, query = %s\n", method, url, query);
    // 有点问题，没想明白
    if(!strcasecmp(method, "GET"))
    {
        Get(client, url, query);
    }
    
    printf("ok\n");
}

/**
 * 每次获取一行报文，行终止的标志为\n或者\r\n
 * 
 * int sockfd: 需要接收的套接字的描述符
 * char *buf: 存放获取行的缓冲区
 * int size: 缓冲区大小
 * 
 * return: 获取的字符数
 * 
 */
int GetLine(int sockfd, char *buf, int size)
{
    char c = '\0';
    int charNum = 0;

    while(charNum < size - 1)
    {
        int n = recv(sockfd, &c, 1, 0);
        if(n > 0)   // 传输成功
        {
            if(c == '\r' || c == '\n')
            {
                // 只要遇到\r，下一个字符默认为\n也就是行终止
                buf[charNum++] = '\n';
                break;
            }
            buf[charNum++] = c;
        }
        else
        {
            buf[charNum++] = '\n';
            break;
        }
    }
    buf[charNum] = '\0';

    return charNum;
}

int main()
{
    // 建立套接字
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
   
    uint16_t port = 4568;
    struct sockaddr_in name;
    name.sin_family = AF_INET;
    name.sin_port = htons(port);    // 主机字节序表示的端口号转化为网络字节序
    name.sin_addr.s_addr = htonl(INADDR_ANY);

    socklen_t nameLen = sizeof(name);

    // 套接字与地址关联
    if(bind(sockfd, (struct sockaddr *)&name, nameLen) == -1)
    {
        printf("bind failed.\n");
        exit(1);
    }

    if(listen(sockfd, 3) == -1)
    {
        printf("listen failed.\n");
        exit(2);
    }

    int clientSock = -1;
    struct sockaddr_in clientName;
    socklen_t clientNameLen = sizeof(clientName);
    pthread_t newThread;
    while((clientSock = accept(sockfd, (struct sockaddr *)&clientName, &clientNameLen)) != -1)
    {
        // 打印客户端信息
        char str[30];
        printf("address : %s\n", inet_ntop(AF_INET, &(clientName.sin_addr), str, 20));
        printf("port : %d\n\n", ntohs(clientName.sin_port));
        
        // 创建新线程，调用AcceptRequest((void *)&clientSock)
        if(pthread_create(&newThread, NULL, (void *)AcceptRequest, (void *)&clientSock) != 0)
        {
            printf("thread create failed\n");
            exit(4);
        }
    }

    exit(0);
}
